pyOASIS - A Python wrapper for OASIS

Authors: Philippe Gambron, Rupert Ford, Andrea Piacentini, Anthony Craig and Sophie Valcke

Copyright (C) 2021 UKRI - STFC for contributions from Philippe Gambron and Rupert Ford
Copyright (C) 2021 CERFACS for contributions from Andrea Piacentini, Anthony Craig and Sophie Valcke 

See the OASIS web site at https://oasis.cerfacs.fr/en/
for  documentaiton and instructions about how to obtain and run the software.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of the 
License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

A copy of the GNU Lesser General Public License, version 3, is supplied
with this program, in the file lgpl-3.0.txt. It is also available at 
<https://www.gnu.org/licenses/lgpl-3.0.html>.
